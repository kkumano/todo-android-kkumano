package com.webserva.wings.android.todo_app

import retrofit2.Call
import retrofit2.http.*

interface TodoApiRequest {
    @GET("todos")
    fun fetchTodos(): Call<TodosGetResponse>

    @POST("todos")
    fun sendTodo(
        @Body todoRequestBody: TodoRequestBody
    ): Call<BaseResponse>

    @PUT("todos/{id}")
    fun updateTodo(
        @Path("id") id: Int,
        @Body todoRequestBody: TodoRequestBody
    ): Call<BaseResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(
        @Path("id") id: Int
    ): Call<BaseResponse>
}