package com.webserva.wings.android.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Todo(
    val id: Int,
    var title: String,
    var detail: String?,
    var date: Date?
) : Parcelable