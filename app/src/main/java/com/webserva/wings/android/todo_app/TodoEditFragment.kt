package com.webserva.wings.android.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.icu.util.Calendar
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {

    private val args by navArgs<TodoEditFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        val todo = args.todo
        if (todo != null) {
            setUpTexts(todo)
        }
        setUpButton(todo?.id)
    }

    private fun setUpViews() {
        text_title_length_counter_denominator.text =
            getString(R.string.denominator_text, TITLE_MAX_LENGTH)
        text_title_length_counter.setText(R.string.initial_molecule_text)
        edit_text_title.addTextChangedListener {
            updateCounter(edit_text_title.text, true)
            updateOKButton()
        }

        text_detail_length_counter_denominator.text =
            getString(R.string.denominator_text, DETAIL_MAX_LENGTH)
        text_detail_length_counter.setText(R.string.initial_molecule_text)
        edit_text_detail.addTextChangedListener {
            updateCounter(edit_text_detail.text, false)
            updateOKButton()
        }

        text_date_picker.setOnClickListener {
            showDatePickerDialog()
        }
    }

    private fun setUpTexts(todo: Todo) {
        edit_text_title.setText(todo.title)
        todo.detail?.let {
            edit_text_detail.setText(todo.detail)
        }
        todo.date?.let {
            val format = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
            text_date_picker.text = format.format(todo.date)
        }
    }

    private fun setUpButton(id: Int?) {
        if (id != null) {
            button_edit_ok.text = getString(R.string.ok_update)
        } else {
            button_edit_ok.text = getString(R.string.ok_register)
            button_edit_ok.isEnabled = false
        }
        button_edit_ok.setOnClickListener {
            CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
                sendTodo(id)
            }
        }
    }

    private fun updateCounter(text: Editable?, isTitleCounter: Boolean) {
        val length = text?.length ?: 0
        val maxLength = if (isTitleCounter) TITLE_MAX_LENGTH else DETAIL_MAX_LENGTH
        val counterColor = if (length > maxLength) Color.RED else Color.GRAY
        val counterTextView = if (isTitleCounter) text_title_length_counter else text_detail_length_counter

        counterTextView.text = "$length"
        counterTextView.setTextColor(counterColor)
    }

    private fun updateOKButton() {
        val titleTextLength = edit_text_title.text.length
        val detailTextLength = edit_text_detail.text.length

        val isOKButtonEnabled = titleTextLength in 1..TITLE_MAX_LENGTH && detailTextLength <= DETAIL_MAX_LENGTH
        button_edit_ok.isEnabled = isOKButtonEnabled
    }

    private fun showDatePickerDialog() {
        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            requireContext(), DatePickerDialog.OnDateSetListener { _, y, m, d ->
                text_date_picker.text = getString(R.string.date_picker_text, y, m + 1, d)
            }, year, month, day
        )
        datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.neutral_button_date_picker)) { _, _ ->
            text_date_picker.text = ""
        }
        datePickerDialog.show()
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = TodoApiClient().todoApiRequest
            val response = if (id != null) {
                request.updateTodo(id, makeBody()).execute()
            } else {
                request.sendTodo(makeBody()).execute()
            }
            withContext(Dispatchers.Main) {
                val body = response.body() ?: return@withContext showErrorDialog(getString(R.string.message_unknown_error))
                onReceiveResponse(body, id != null)
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.message_unknown_error))
            }
        }
    }

    private fun onReceiveResponse(response: BaseResponse, isUpdate: Boolean) {
        if (response.errorCode != 0) {
            showErrorDialog(response.errorMessage)
            return
        }
        val resourceID =
            if (isUpdate) R.string.message_todo_updated else R.string.message_todo_registered
        onSuccess(getString(resourceID))
    }

    private fun makeBody(): TodoRequestBody {
        val title = edit_text_title.text.toString()
        val detail = edit_text_detail?.text?.toString()
        val format = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        val date =
            if (text_date_picker.text.isNullOrEmpty()) null else format.parse(text_date_picker.text.toString())

        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(message: String) {
        findNavController().navigateUp()
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        private const val TITLE_MAX_LENGTH = 100
        private const val DETAIL_MAX_LENGTH = 1000
        private const val DATE_PATTERN = "yyyy/M/d"
    }
}