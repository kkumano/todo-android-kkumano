package com.webserva.wings.android.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.*

class TodoListFragment : Fragment() {

    private lateinit var adapter: RecyclerAdapter
    private var isDeleteMode = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = RecyclerAdapter(::onClickRow)
        val decorator = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)

        recycler_todo.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(activity)
            it.addItemDecoration(decorator)
            it.adapter = adapter
        }
        CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_create ->
                goToEditFragment()
            R.id.menu_delete ->
                changeDeleteMode(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = TodoApiClient().todoApiRequest.fetchTodos().execute()
            val body = response.body() ?: throw Exception()
            withContext(Dispatchers.Main) {
                when (body.errorCode) {
                    0 -> adapter.todoList = body.todos!!
                    else -> showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.message_unknown_error))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = TodoApiClient().todoApiRequest.deleteTodo(id).execute()
            val body = response.body() ?: throw Exception()
            when (body.errorCode) {
                0 -> fetchTodoList()
                else -> {
                    withContext(Dispatchers.Main) {
                        showErrorDialog(body.errorMessage)
                    }
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.message_unknown_error))
            }
        }
    }

    private fun onClickRow(todo: Todo) {
        if (isDeleteMode) {
            showCheckDeleteDialog(todo)
        } else {
            goToEditFragment(todo)
        }
    }

    private fun goToEditFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionToEdit(todo)
        findNavController().navigate(action)
    }

    private fun changeDeleteMode(item: MenuItem) {
        isDeleteMode = !isDeleteMode
        val resourceDrawable =
            if (isDeleteMode) R.drawable.ic_done_24px else R.drawable.ic_delete_outline_24px
        item.setIcon(resourceDrawable)
    }

    private fun showCheckDeleteDialog(todo: Todo) {
        AlertDialog.Builder(context)
            .setMessage(getString(R.string.message_check_delete_dialog, todo.title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
                    deleteTodo(todo.id)
                }
            }
            .show()
    }
}